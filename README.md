# 说明文档


## 一、简介

  :fire: **本项目基于本人的mts模板工程，[点击这里查看](https://gitee.com/haifeng_org/mts),本文注重整体技术解决方案，前端工程这里我只提供个具有登录、刷新Token等框架层相关功能，其他具体对应菜单的增删改查就不写了，可以自己实现，有问题欢迎交流** 

### 1.1、设计思路
大致如图：
![输入图片说明](https://images.gitee.com/uploads/images/2020/0502/110800_9b68fbb9_7468166.png "TIM截图20200502110714.png")
### 1.2、项目版本及功能

Spring Boot ：2.2.5.RELEASE
Spring Cloud：Hoxton.SR3
Spring Cloud Alibaba：2.2.1.RELEASE
spring-cloud-starter-alibaba-seata：2.2.1


 :point_right:  **已实现以下功能：** 

1、使用Nacos 1.2.1做服务注册、配置中心、消息总线（Nginx代理3台Nacos，由于每台Nacos默认内存2g，注意适当修改）。

2、负载均衡：网关使用Sentinel自带的Ribbon，服务间调用使用OpenFeign自带的Ribbon。

3、使用Spring Cloud Gateway作为网关，网关主要用作路由、鉴权、限流、降级、熔断、接口文档聚合等作用。

4、服务降级、熔断、限流使用的是Sentinel 1.7.1，动态读取Nacos中的网关规则

5、权限框架使用的是Spring Security OAuth2.0 基于JWT令牌方式。

6、分布式事务：Seata 1.1.0。

7、分布式ID：SnowFlake。

8、接口文档：Swagger，通过网关聚合所有微服务。

9、持久层框架采用MyBatis Plus 3.3.1，简便开发，提高效率。

10、数据库采用高可用一主一备两从的方案，使用MyCat中间件来集成：MySQL 5.7.25，连接池：HikariCP。

11、缓存使用高可用的Redis哨兵模式：一主两从。

12、消息队列集成RabbitMQ。

### 1.4、模块介绍

```txt
|--wdsh 父模块
|----wdsh-api ：api模块集合
|--------wdsh-api-sys ：系统API
|----wdsh-auth ：认证授权模块
|----wdsh-business ：业务模块集合
|--------mts-business-sys ：系统业务
|----wdsh-common ：公共模块集合
|--------wdsh-common-base ：基础
|--------wdsh-common-datasource ：数据源，分布式事务，mybatis
|--------wdsh-common-redis ：缓存Redis
|--------wdsh-common-rabbit ：消息队列
|----wdsh-gateway ：网关





|--monitor
|----monitor-api ：openfeign api模块集合
|--------monitor-api-monitor ：监测api
|----monitor-web ：服务集合
|--------monitor-web-monitor ：监测服务
|--------monitor-web-mqtt ：iot传输服务
|--------monitor-web-app ：app服务
|----monitor-common ：公共模块集合
|--------monitor-common-bom ：版本管理模块
|--------monitor-common-core ：核心基础模块
|--------monitor-common-datasource ：数据源模块
|--------monitor-common-redis ：Redis模块
|--------monitor-common-log ：日志模块
|--------monitor-common-openfeign ：openfeign模块
|--------monitor-common-oss ：文件存储模块
|--------monitor-common-kafka ：kafka模块
|----monitor-gateway ：在线监测网关
```
### 1.5、截图
 **1、nacos中存储流控规则以及微服务的动态配置** 
![输入图片说明](https://images.gitee.com/uploads/images/2020/0512/231037_2bc7e226_7468166.png "1.png")
 **2、nacos采用Nginx集群3台，达到高可用** 
![输入图片说明](https://images.gitee.com/uploads/images/2020/0512/231824_73227f03_7468166.png "2.png")
 **3、sentinel控制台实时动态生效在nacos中配置的流控规则** 
![输入图片说明](https://images.gitee.com/uploads/images/2020/0512/231948_2ccf0ec8_7468166.png "3.png")
 **4、MySQL高可用集群，采用端口8066的MyCat作为中间件集成** 
![输入图片说明](https://images.gitee.com/uploads/images/2020/0512/232452_0d921ead_7468166.png "6.png")
 **5、Redis高可用哨兵模式一主两从** 
![输入图片说明](https://images.gitee.com/uploads/images/2020/0512/232526_6877b00a_7468166.png "7.png")
 **6、消息队列RabbitMQ演示** 
![输入图片说明](https://images.gitee.com/uploads/images/2020/0512/232654_26cbfa0b_7468166.png "8.png")
 **7、接口文档** 
![输入图片说明](https://images.gitee.com/uploads/images/2020/0512/233049_6aba6206_7468166.png "9.png")
 **8、前端登录页面** 
![输入图片说明](https://images.gitee.com/uploads/images/2020/0512/232000_aacf85c7_7468166.png "4.png")
 **9、前端页面效果** 
![输入图片说明](https://images.gitee.com/uploads/images/2020/0512/232042_71e25b43_7468166.png "5.png")


## 二、联系与赞助

联系QQ：184377902   

邮箱：haifeng_personal@aliyun.com

<img src="https://images.gitee.com/uploads/images/2019/1211/193635_a36a4999_5541360.png" style="zoom: 25%;" />

<img src="https://images.gitee.com/uploads/images/2019/1211/193648_a230b776_5541360.png" style="zoom: 15%;" />
