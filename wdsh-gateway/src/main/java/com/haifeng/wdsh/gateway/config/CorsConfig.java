package com.haifeng.wdsh.gateway.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.reactive.CorsWebFilter;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;

/**
 * <p>
 *  跨域配置类
 * </p>
 *
 * @author: Haifeng
 * @date: 2020-05-09
 */
@Configuration
public class CorsConfig {

    @Bean
    public CorsWebFilter corsWebFilter(){
        String path = "/**";
        CorsConfiguration corsConfiguration = new CorsConfiguration();
        // 允许访问的客户端域名
        corsConfiguration.addAllowedOrigin("*");
        corsConfiguration.setAllowCredentials(true);
        // 允许所有请求方式:Get、Post、Put、Delete
        corsConfiguration.addAllowedMethod("*");
        corsConfiguration.addAllowedHeader("*");
        UrlBasedCorsConfigurationSource urlBasedCorsConfigurationSource = new UrlBasedCorsConfigurationSource();
        urlBasedCorsConfigurationSource.registerCorsConfiguration(path,corsConfiguration);
        return new CorsWebFilter(urlBasedCorsConfigurationSource);
    }
}
