package com.haifeng.wdsh.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * <p>
 *  主类
 * </p>
 * @author: Haifeng
 * @date: 2020/05/07
 */
@EnableDiscoveryClient
@SpringBootApplication(scanBasePackages = {"com.haifeng.wdsh"})
public class WdshGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(WdshGatewayApplication.class, args);
    }

}
