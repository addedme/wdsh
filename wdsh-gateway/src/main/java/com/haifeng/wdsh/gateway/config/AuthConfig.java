package com.haifeng.wdsh.gateway.config;

import com.haifeng.wdsh.gateway.properties.AuthProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * <p>
 *  权限配置类
 * </p>
 *
 * @author: Haifeng
 * @date: 2020-05-06
 */
@Configuration
@EnableConfigurationProperties({AuthProperties.class})
public class AuthConfig {

}
