package com.haifeng.wdsh.gateway.properties;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Arrays;
import java.util.List;

/**
 * <p>
 *  网关权限配置项
 * </p>
 *
 * @author: Haifeng
 * @date: 2020-05-03
 */
@Slf4j
@Data
@ConfigurationProperties(prefix = "gateway.oauth")
public class AuthProperties {

    /**
     * 不需要鉴权的资源
     */
    private List<String> passAuth = Arrays.asList("/auth/**","/**/v2/api-docs","/**/v2/api-docs-ext","/**/doc.html");

    /**
     * 微服务前缀
     */
    private List<String> servicePrefix = Arrays.asList("/sys");

    /**
     * false-使用JWT自包含的权限信息验证，true-通过Redis获取最新权限验证
     */
    private Boolean useRedis = false;

}
