package com.haifeng.wdsh.api.sys.fallback;

import com.haifeng.wdsh.common.base.exception.ErrorMsg;
import com.haifeng.wdsh.common.base.util.Rest;
import com.haifeng.wdsh.common.base.vo.UserVo;
import com.haifeng.wdsh.api.sys.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

/**
 * <p>
 *
 * </p>
 *
 * @author: Haifeng
 * @date: 2020-05-06
 */
@Slf4j
@Component
public class UserFallbackImpl implements UserService {

    /**
     * 根据用户名获取用户信息
     * @param username 用户名
     * @return
     */
    @Override
    public Rest<UserVo> getUserByUsername(String username) {
        log.warn("服务降级处理...UserFallback...getUserByUsername()");
        return Rest.error(HttpStatus.SERVICE_UNAVAILABLE.value(),ErrorMsg.SERVICE_UNAVAILABLE);
    }
}
