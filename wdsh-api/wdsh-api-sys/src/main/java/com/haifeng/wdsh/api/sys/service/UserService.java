package com.haifeng.wdsh.api.sys.service;

import com.haifeng.wdsh.common.base.util.Rest;
import com.haifeng.wdsh.common.base.vo.UserVo;
import com.haifeng.wdsh.api.sys.fallback.UserFallbackImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * <p>
 *
 * </p>
 *
 * @author: Haifeng
 * @date: 2020-05-06
 */
@FeignClient(value = "wdsh-business-sys",fallback = UserFallbackImpl.class)
public interface UserService {

    /**
     * 根据用户名获取用户信息
     * @param username 用户名
     * @return
     */
    @GetMapping("/user/getUserByUsername")
    Rest<UserVo> getUserByUsername(@RequestParam("username") String username);
}
