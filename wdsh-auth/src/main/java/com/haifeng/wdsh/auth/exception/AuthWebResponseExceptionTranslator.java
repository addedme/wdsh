package com.haifeng.wdsh.auth.exception;


import com.haifeng.wdsh.common.base.constants.Constants;
import com.haifeng.wdsh.common.base.exception.ErrorMsg;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.security.oauth2.provider.error.WebResponseExceptionTranslator;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 *  自定义异常转换器
 * </p>
 *
 * @author: Haifeng
 * @date: 2020-05-06
 */
@Component
@AllArgsConstructor
public class AuthWebResponseExceptionTranslator implements WebResponseExceptionTranslator {

    @Override
    public ResponseEntity translate(Exception e) throws Exception {
        Map map = new HashMap(16);
        map.put("data",null);
        map.put("code",401);
        map.put("success",false);
        if (!(e instanceof OAuth2Exception)){
            map.put("message",e.getMessage());
            return ResponseEntity.ok(map);
        }
        OAuth2Exception exception = (OAuth2Exception) e;
        String message = exception.getMessage();
        if (message.contains(Constants.INVALID_REFRESH_TOKEN)){
            message = ErrorMsg.INVALID_REFRESH_TOKEN;
        }else if (message.contains(Constants.INVALID_AUTHORIZATION_CODE)){
            message = ErrorMsg.INVALID_AUTHORIZATION_CODE;
        }else if (message.contains(Constants.BAD_CREDENTIALS)){
            message = ErrorMsg.BAD_CREDENTIALS;
        }else if (message.contains(Constants.TOKEN_WAS_NOT_RECOGNISED)){
            message = ErrorMsg.TOKEN_WAS_NOT_RECOGNISED;
        }else if (message.contains(Constants.INVALID_SCOPE)){
            message = ErrorMsg.INVALID_SCOPE;
        }else if (message.contains(Constants.UNSUPPORTED_GRANT_TYPE)){
            message = ErrorMsg.UNSUPPORTED_GRANT_TYPE;
        }else if (message.contains(Constants.MISSING_GRANT_TYPE)){
            message = ErrorMsg.MISSING_GRANT_TYPE;
        }else if (message.contains(Constants.AN_AUTHORIZATION_CODE_MUST_BE_SUPPLIED)){
            message = ErrorMsg.AN_AUTHORIZATION_CODE_MUST_BE_SUPPLIED;
        }else if (message.contains(Constants.CANNOT_CONVERT_ACCESS_TOKEN_TO_JSON)){
            message = ErrorMsg.CANNOT_CONVERT_ACCESS_TOKEN_TO_JSON;
        }else if (message.contains(Constants.REQUIRED_STRING_PARAMETER_TOKEN_IS_NOT_PRESENT)){
            message = ErrorMsg.REQUIRED_STRING_PARAMETER_TOKEN_IS_NOT_PRESENT;
        }else if (message.contains(Constants.REDIRECT_URI_MISMATCH)){
            message = ErrorMsg.REDIRECT_URI_MISMATCH;
        }
        map.put("message",message);
        if (exception.getAdditionalInformation()!=null) {
            for (Map.Entry<String, String> entry : exception.getAdditionalInformation().entrySet()) {
                String key = entry.getKey();
                String add = entry.getValue();
                map.put(key,add);
            }
        }
        return ResponseEntity.ok(map);
    }
}

