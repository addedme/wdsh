package com.haifeng.wdsh.auth.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * <p>
 *  Web安全配置
 * </p>
 *
 * @author: Haifeng
 * @date: 2020-05-06
 */
@Order(2)
@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Value("${auth.httpSecurityMatch}")
    private String[] httpSecurityMatch;

    @Value("${auth.noAuth}")
    private String[] noAuth;

    /**
     * 配置加密方式BCryptPasswordEncoder
     * @return
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    @Bean("authenticationManagerBean")
    public AuthenticationManager authenticationManagerBean() throws Exception{
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.formLogin()
                .and()
                .requestMatchers()
                .antMatchers(httpSecurityMatch)
                .and()
                .authorizeRequests()
                .antMatchers(noAuth).permitAll()
                .anyRequest().authenticated()
                .and()
                .csrf().disable();
    }
}
