package com.haifeng.wdsh.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * <p>
 *  入口
 * </p>
 * @author: Haifeng
 * @date: 2020/5/6
 */
@EnableFeignClients(basePackages = {"com.haifeng.wdsh.api.sys"})
@EnableDiscoveryClient
@SpringBootApplication(scanBasePackages = {"com.haifeng.wdsh"})
public class WdshAuthApplication {

    public static void main(String[] args) {
        SpringApplication.run(WdshAuthApplication.class, args);
    }

}
