package com.haifeng.wdsh.common.redis.config;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

/**
 * <p>
 *  分布式锁
 * </p>
 *
 * @author: Haifeng
 * @date: 2020-05-06
 */
@Configuration
public class RedissonConfig {

    @Bean(destroyMethod = "shutdown")
    public RedissonClient redissonClient(){
        Config config = null;
        // 从配置文件中创建config
        java.io.InputStream inputStream = null;
        try {
            inputStream = this.getClass().getResourceAsStream("/redisson.yml");
            config = Config.fromYAML(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        config.useSentinelServers();
        return Redisson.create(config);
    }
}
