package com.haifeng.wdsh.common.redis.utils;

import com.haifeng.wdsh.common.base.util.JsonUtil;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.concurrent.TimeUnit;

/**
 * <p>
 * Redis工具类
 * </p>
 *
 * @author: Haifeng
 * @date: 2020-05-06
 */
@Slf4j
@Component
@AllArgsConstructor
public class RedisUtil {

    private final StringRedisTemplate stringRedisTemplate;

    /**
     * 指定缓存失效时间
     *
     * @param key  键
     * @param time 时间(秒)
     * @return
     */
    public Boolean expire(String key, long time) {
        try {
            if (time > 0) {
                stringRedisTemplate.expire(key, time, TimeUnit.SECONDS);
            }
            return true;
        } catch (Exception e) {
            log.error("Redis操作异常："+e.getMessage());
            return false;
        }
    }

    /**
     * 根据key 获取过期时间
     *
     * @param key 键 不能为null
     * @return 时间(秒) 返回0代表为永久有效
     */
    public Long getExpire(String key) {
        try {
            return stringRedisTemplate.getExpire(key, TimeUnit.SECONDS);
        }catch (Exception e){
            log.error("Redis操作异常："+e.getMessage());
            return null;
        }
    }

    /**
     * 判断key是否存在
     *
     * @param key 键
     * @return true 存在 false不存在
     */
    public Boolean hasKey(String key) {
        try {
            return stringRedisTemplate.hasKey(key);
        } catch (Exception e) {
            log.error("Redis操作异常："+e.getMessage());
            return false;
        }
    }

    /**
     * 删除缓存
     *
     * @param key 可以传一个值 或多个
     */
    public Boolean delete(String... key) {
        try {
            if (key != null && key.length > 0) {
                if (key.length == 1) {
                    stringRedisTemplate.delete(key[0]);
                } else {
                    stringRedisTemplate.delete(CollectionUtils.arrayToList(key));
                }
                return true;
            }
            return false;
        }catch (Exception e){
            log.error("Redis操作异常："+e.getMessage());
            return false;
        }
    }

    /**
     * 普通缓存获取
     *
     * @param key 键
     * @return 值
     */
    public String get(String key) {
        try {
            if (StringUtils.isEmpty(key) || !hasKey(key)){
                return null;
            }
            return stringRedisTemplate.opsForValue().get(key);
        }catch (Exception e){
            log.error("Redis操作异常："+e.getMessage());
            return null;
        }
    }

    /**
     * 普通缓存放入
     *
     * @param key   键
     * @param value 值
     * @return true成功 false失败
     */
    public Boolean setKey(String key, Object value) {
        try {
            stringRedisTemplate.opsForValue().set(key, JsonUtil.toJsonString(value));
            return true;
        } catch (Exception e) {
            log.error("Redis操作异常："+e.getMessage());
            return false;
        }
    }

    /**
     * 追加
     *
     * @param key   键
     * @param value 值
     * @return true成功 false失败
     */
    public Boolean append(String key, Object value) {
        try {
            stringRedisTemplate.opsForValue().append(key, JsonUtil.toJsonString(value));
            return true;
        } catch (Exception e) {
            log.error("Redis操作异常："+e.getMessage());
            return false;
        }
    }

    /**
     * 普通缓存放入并设置时间
     *
     * @param key   键
     * @param value 值
     * @param time  时间(秒) time要大于0 如果time小于等于0 将设置无限期
     * @return true成功 false 失败
     */
    public Boolean setKeyWithExpire(String key, Object value, long time) {
        try {
            stringRedisTemplate.opsForValue().set(key, JsonUtil.toJsonString(value), time, TimeUnit.SECONDS);
            return true;
        } catch (Exception e) {
            log.error("Redis操作异常："+e.getMessage());
            return false;
        }
    }
}
