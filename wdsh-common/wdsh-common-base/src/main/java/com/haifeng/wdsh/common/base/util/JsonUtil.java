package com.haifeng.wdsh.common.base.util;

import com.alibaba.fastjson.JSON;

/**
 * <p>
 *  JSON工具类
 * </p>
 *
 * @author: Haifeng
 * @date: 2020-05-05
 */
public class JsonUtil {

    /**
     * 对象转为Json字符串，不忽略null值
     * @param obj
     * @return
     */
    public static String toJsonString(Object obj){

        return JSON.toJSONString(obj, JsonSerializer.serializerFeatures);
    }
}
