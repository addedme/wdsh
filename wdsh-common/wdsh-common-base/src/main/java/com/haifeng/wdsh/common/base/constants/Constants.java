package com.haifeng.wdsh.common.base.constants;

/**
 * <p>
 *  常量类
 * </p>
 *
 * @author: Haifeng
 * @date: 2020-05-05
 */
public class Constants {

    public static final String USER_ = "wdsh_user_";
    public static final String JWT_SIGN_KEY = "wdsh";
    public static final String REMOTEADDRESS = "192.168.198.100:7777";
    public static final String GROUPID = "wdsh-gateway";
    public static final String DATAIDAPI = "wdsh-gateway-customer-api";
    public static final String DATAIDRULE = "wdsh-gateway-flow-rules";
    public static final String AUTH_PROJECT = "wdsh-auth";
    public static final String HEADER_NAME = "X-Forwarded-Prefix";
    public static final String SWAGGERPV2 = "/v2/api-docs";
    public static final String AUTHORIZATION_HEADER_NAME = "Authorization";
    public static final String USER_INFO = "X-User";
    public static final String BEARER = "Bearer ";

    public static final String INVALID_REFRESH_TOKEN = "Invalid refresh token";
    public static final String INVALID_AUTHORIZATION_CODE = "Invalid authorization code";
    public static final String BAD_CREDENTIALS = "Bad credentials";
    public static final String TOKEN_WAS_NOT_RECOGNISED = "Token was not recognised";
    public static final String INVALID_SCOPE = "Invalid scope";
    public static final String UNSUPPORTED_GRANT_TYPE = "Unsupported grant type";
    public static final String MISSING_GRANT_TYPE = "Missing grant type";
    public static final String AN_AUTHORIZATION_CODE_MUST_BE_SUPPLIED = "An authorization code must be supplied";
    public static final String CANNOT_CONVERT_ACCESS_TOKEN_TO_JSON = "Cannot convert access token to JSON";
    public static final String REQUIRED_STRING_PARAMETER_TOKEN_IS_NOT_PRESENT = "Required String parameter 'token' is not present";
    public static final String REDIRECT_URI_MISMATCH = "Redirect URI mismatch.";

}
