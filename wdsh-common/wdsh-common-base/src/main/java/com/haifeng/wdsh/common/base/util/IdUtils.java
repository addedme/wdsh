package com.haifeng.wdsh.common.base.util;

import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.IdUtil;
import com.haifeng.wdsh.common.base.enums.IdEnum;

/**
 * <p>
 *  分布式id
 * </p>
 *
 * @author: Haifeng
 * @date: 2020-05-05
 */
public class IdUtils {

    /**
     * 全局唯一ID
     * @param idEnum
     */
    public static Long getId(IdEnum idEnum){
        Snowflake snowflake = IdUtil.getSnowflake(idEnum.getWorkerId(), idEnum.getDatacenterId());
        return snowflake.nextId();
    }
}
