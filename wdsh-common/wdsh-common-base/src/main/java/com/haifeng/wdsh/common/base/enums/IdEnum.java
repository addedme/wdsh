package com.haifeng.wdsh.common.base.enums;

/**
 * <p>
 *  分布式ID枚举类
 * </p>
 *
 * @author: Haifeng
 * @date: 2020-05-05
 */
public enum IdEnum {

    /**
     * 系统模块
     */
    WDSH_SERVICE_SYS(1L,1L);

    private Long workerId;
    private Long datacenterId;


    IdEnum(Long workerId, Long datacenterId) {
        this.workerId = workerId;
        this.datacenterId = datacenterId;
    }

    public Long getWorkerId() {
        return workerId;
    }

    public Long getDatacenterId() {
        return datacenterId;
    }

}
