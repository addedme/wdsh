package com.haifeng.wdsh.common.base.exception;

/**
 * <p>
 *  提示消息
 * </p>
 *
 * @author: Haifeng
 * @date: 2020-05-05
 */
public class ErrorMsg {

    /**
     *  成功
     */
    public static final String SUCCESS = "成功";
    /**
     *  服务器异常，请稍后再试
     */
    public static final String SERVER_FAIL = "服务器异常，请稍后再试";
    /**
     *  服务不可用，请稍后再试
     */
    public static final String SERVICE_UNAVAILABLE = "服务不可用，请稍后再试";
    /**
     *  refresh_token不正确！
     */
    public static final String INVALID_REFRESH_TOKEN = "refresh_token不正确！";

    /**
     *  请求的授权码不正确！
     */
    public static final String INVALID_AUTHORIZATION_CODE = "授权码不正确！";

    /**
     *  客户端凭证不正确！
     */
    public static final String BAD_CREDENTIALS = "客户端凭证不正确！";

    /**
     *  令牌不正确或者已失效！
     */
    public static final String TOKEN_WAS_NOT_RECOGNISED = "令牌不正确或者已失效！";

    /**
     *  请求的scope不正确！
     */
    public static final String INVALID_SCOPE = "请求的scope不正确！";
    /**
     *  不支持该授权方式！
     */
    public static final String UNSUPPORTED_GRANT_TYPE = "不支持该授权方式！";
    /**
     *  请求缺少授权类型！
     */
    public static final String MISSING_GRANT_TYPE = "请求缺少授权类型！";
    /**
     *  请求缺少授权码！
     */
    public static final String AN_AUTHORIZATION_CODE_MUST_BE_SUPPLIED = "请求缺少授权码！";
    /**
     *  您没有权限访问此资源！
     */
    public static final String NO_PERMISSION = "您没有权限访问此资源！";
    /**
     *  该令牌不正确！
     */
    public static final String CANNOT_CONVERT_ACCESS_TOKEN_TO_JSON = "该令牌不正确！";
    /**
     *  Head中缺少Authorization参数！
     */
    public static final String REQUIRED_STRING_PARAMETER_TOKEN_IS_NOT_PRESENT = "Head中缺少Authorization参数！";
    /**
     *  Redirect URI不正确！
     */
    public static final String REDIRECT_URI_MISMATCH = "Redirect URI不正确！";
    /**
     *  用户不存在：%s
     */
    public static final String USER_NOT_FOUND = "用户不存在：%s";
    /**
     *  Authorization参数格式不正确，请确认以Bearer 开头
     */
    public static final String NOT_START_WITH_BEARER = "Authorization参数格式不正确，请确认以Bearer 开头";
    /**
     *  令牌已过期
     */
    public static final String TOKEN_EXPIRED = "令牌已过期";
}
