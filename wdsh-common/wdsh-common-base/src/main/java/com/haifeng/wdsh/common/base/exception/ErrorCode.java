package com.haifeng.wdsh.common.base.exception;

/**
 * <p>
 *  错误码
 * </p>
 *
 * @author: Haifeng
 * @date: 2020-05-05
 */
public class ErrorCode {
    /**
     *  成功
     */
    public static final Integer SUCCESS = 200;
    /**
     *  服务器异常
     */
    public static final Integer SERVER_FAIL = 500;

    /**
     *  token过期
     */
    public static final Integer TOKEN_EXPIRED = 50000;

}
