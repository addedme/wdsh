package com.haifeng.wdsh.common.base.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 权限表
 * </p>
 *
 * @author Haifeng
 * @since 2020-05-06
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="Permission对象", description="权限表")
public class PermissionVo implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "权限编号")
    private String permissionCode;

    @ApiModelProperty(value = "权限名称")
    private String permissionName;

    @ApiModelProperty(value = "权限url")
    private String permissionUrl;

    @ApiModelProperty(value = "权限在当前目录下的排序")
    private Integer permissionSort;

    @ApiModelProperty(value = "权限类型：1-菜单；2-按钮")
    private Boolean permissionType;

    @ApiModelProperty(value = "父权限")
    private Long parentId;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createDatetime;

    @ApiModelProperty(value = "更新时间")
    private LocalDateTime updateDatetime;

    @ApiModelProperty(value = "是否删除，0-未删除，1-已删除")
    private Boolean deleteFlag;

    @ApiModelProperty(value = "版本号")
    private Integer version;
}
