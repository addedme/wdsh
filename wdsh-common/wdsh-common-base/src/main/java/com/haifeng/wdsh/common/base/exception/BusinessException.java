package com.haifeng.wdsh.common.base.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * <p>
 *  自定义业务异常类
 * </p>
 *
 * @author: Haifeng
 * @date: 2020-05-05
 */
@Data
@ToString
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class BusinessException extends RuntimeException {

    private Integer errCode;

    public BusinessException(Integer errCode) {
        this.errCode = errCode;
    }

    public BusinessException(Integer errCode, String message) {
        super(message);
        this.errCode = errCode;
    }

    public BusinessException(Integer errCode, String message, Throwable cause) {
        super(message, cause);
        this.errCode = errCode;
    }

    public BusinessException(Integer errCode, Throwable cause) {
        super(cause);
        this.errCode = errCode;
    }

    public BusinessException(Integer errCode, String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.errCode = errCode;
    }

    private static final long serialVersionUID = 1L;
}
