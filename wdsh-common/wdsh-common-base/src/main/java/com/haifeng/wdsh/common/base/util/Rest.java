package com.haifeng.wdsh.common.base.util;

import com.haifeng.wdsh.common.base.exception.ErrorCode;
import com.haifeng.wdsh.common.base.exception.ErrorMsg;
import lombok.*;

import java.io.Serializable;

/**
 * <p>
 *  Rest通用返回类
 * </p>
 *
 * @author: Haifeng
 * @date: 2020-05-05
 */
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class Rest<T> implements Serializable {

    private Boolean success = true;
    private T data;
    private Integer code;
    private String message;

    /**
     * 成功返回(默认)
     * @param data
     * @return
     */
    public static Rest success(Object data){
        return new Rest(true, data, ErrorCode.SUCCESS, ErrorMsg.SUCCESS);
    }

    /**
     * 失败返回
     * @param errCode
     * @param message
     * @return
     */
    public static Rest error(Integer errCode, String message){
        return new Rest(false,null,errCode,message);
    }
}
