package com.haifeng.wdsh.common.base.config;

import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;

/**
 * <p>
 *  缓存Key生成类
 * </p>
 *
 * @author: Haifeng
 * @date: 2020-05-05
 */
@Configuration
public class CacheConfig {

    @Bean("cacheKeyGenerator")
    public KeyGenerator cacheKeyGenerator(){
        return (target, method, params) ->
                target.getClass().getName()+"_"
                        +method.getName()+"_"
                        + Arrays.asList(params);
    }
}
