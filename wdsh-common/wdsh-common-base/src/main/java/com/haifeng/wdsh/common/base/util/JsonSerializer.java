package com.haifeng.wdsh.common.base.util;
import com.alibaba.fastjson.serializer.SerializerFeature;

/**
 * <p>
 *  JsonSerializer
 * </p>
 *
 * @author: Haifeng
 * @date: 2020-05-05
 */
public class JsonSerializer {

    public static SerializerFeature[] serializerFeatures = new SerializerFeature[] {
            SerializerFeature.PrettyFormat,
            SerializerFeature.SortField,
            SerializerFeature.WriteMapNullValue
    };
}
