package com.haifeng.wdsh.common.datasource.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 部门表
 * </p>
 *
 * @author Haifeng
 * @since 2020-05-06
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tb_dept")
@ApiModel(value="Dept对象", description="部门表")
public class Dept implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "部门编号")
    @TableField("dept_code")
    private String deptCode;

    @ApiModelProperty(value = "部门名称")
    @TableField("dept_name")
    private String deptName;

    @ApiModelProperty(value = "上级部门")
    @TableField("parent_id")
    private Long parentId;

    @ApiModelProperty(value = "部门在当前层级下的顺序")
    @TableField("dept_sort")
    private Integer deptSort;

    @ApiModelProperty(value = "部门层级")
    @TableField("dept_level")
    private Integer deptLevel;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "创建时间")
    @TableField("create_datetime")
    private LocalDateTime createDatetime;

    @ApiModelProperty(value = "更新时间")
    @TableField("update_datetime")
    private LocalDateTime updateDatetime;

    @ApiModelProperty(value = "是否删除，0-未删除，1-已删除")
    @TableField("delete_flag")
    private Boolean deleteFlag;

    @ApiModelProperty(value = "版本号")
    private Integer version;


}
