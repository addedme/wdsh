package com.haifeng.wdsh.common.datasource.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 权限表
 * </p>
 *
 * @author Haifeng
 * @since 2020-05-06
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tb_permission")
@ApiModel(value="Permission对象", description="权限表")
public class Permission implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "权限编号")
    @TableField("permission_code")
    private String permissionCode;

    @ApiModelProperty(value = "权限名称")
    @TableField("permission_name")
    private String permissionName;

    @ApiModelProperty(value = "权限url")
    @TableField("permission_url")
    private String permissionUrl;

    @ApiModelProperty(value = "权限在当前目录下的排序")
    @TableField("permission_sort")
    private Integer permissionSort;

    @ApiModelProperty(value = "权限类型：1-菜单；2-按钮")
    @TableField("permission_type")
    private Boolean permissionType;

    @ApiModelProperty(value = "父权限")
    @TableField("parent_id")
    private Long parentId;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "创建时间")
    @TableField("create_datetime")
    private LocalDateTime createDatetime;

    @ApiModelProperty(value = "更新时间")
    @TableField("update_datetime")
    private LocalDateTime updateDatetime;

    @ApiModelProperty(value = "是否删除，0-未删除，1-已删除")
    @TableField("delete_flag")
    private Boolean deleteFlag;

    @ApiModelProperty(value = "版本号")
    private Integer version;
}
