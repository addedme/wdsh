package com.haifeng.wdsh.common.datasource.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

/**
 * <p>
 * 用户表
 * </p>
 *
 * @author Haifeng
 * @since 2020-05-06
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tb_user")
@ApiModel(value="User对象", description="用户表")
public class User implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "用户名")
    private String username;

    @JSONField(serialize = false)
    @ApiModelProperty(value = "密码，加密存储")
    private String password;

    @ApiModelProperty(value = "部门id")
    @TableField("dept_id")
    private Long deptId;

    @ApiModelProperty(value = "类型：0-男；1-女")
    private Boolean gender;

    @ApiModelProperty(value = "手机号")
    @TableField("mobile_phone")
    private String mobilePhone;

    @ApiModelProperty(value = "微信号")
    private String wechat;

    @ApiModelProperty(value = "邮箱")
    private String email;

    @ApiModelProperty(value = "住址")
    private String address;

    @ApiModelProperty(value = "身份证号")
    @TableField("id_card_no")
    private String idCardNo;

    @ApiModelProperty(value = "账户是否过期：0-过期；1-未过期")
    @TableField("account_non_expired")
    private Boolean accountNonExpired;

    @ApiModelProperty(value = "密码是否过期：0-过期；1-未过期")
    @TableField("credentials_non_expired")
    private Boolean credentialsNonExpired;

    @ApiModelProperty(value = "是否锁定：0-锁定；1-未锁定")
    @TableField("account_non_locked")
    private Boolean accountNonLocked;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "是否可用：0-不可用；1-可用")
    private Boolean enabled;

    @ApiModelProperty(value = "创建时间")
    @TableField("create_datetime")
    private LocalDateTime createDatetime;

    @ApiModelProperty(value = "更新时间")
    @TableField("update_datetime")
    private LocalDateTime updateDatetime;

    @ApiModelProperty(value = "是否删除，0-未删除，1-已删除")
    @TableField("delete_flag")
    private Boolean deleteFlag;

    @ApiModelProperty(value = "版本号")
    private Integer version;

    @TableField(exist = false)
    @ApiModelProperty(value = "权限集合")
    private Set<Permission> permissions = new HashSet<>();
}
