package com.haifeng.wdsh.business.sys.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 权限表 前端控制器
 * </p>
 *
 * @author Haifeng
 * @since 2020-05-06
 */
@RestController
@RequestMapping("/permission")
public class PermissionController extends BaseController {

}

