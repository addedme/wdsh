package com.haifeng.wdsh.business.sys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haifeng.wdsh.business.sys.mapper.DeptMapper;
import com.haifeng.wdsh.business.sys.service.DeptService;
import com.haifeng.wdsh.common.datasource.entity.Dept;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 部门表 服务实现类
 * </p>
 *
 * @author Haifeng
 * @since 2020-05-06
 */
@Service
public class DeptServiceImpl extends ServiceImpl<DeptMapper, Dept> implements DeptService {

}
