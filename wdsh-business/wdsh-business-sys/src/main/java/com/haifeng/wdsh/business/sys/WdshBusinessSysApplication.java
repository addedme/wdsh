package com.haifeng.wdsh.business.sys;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * <p>
 *  入口
 * </p>
 * @author: Haifeng
 * @date: 2020/5/7
 */
@MapperScan(basePackages = {"com.haifeng.wdsh.business.sys.mapper"})
@EnableDiscoveryClient
@SpringBootApplication(scanBasePackages = {"com.haifeng.wdsh"})
public class WdshBusinessSysApplication {

    public static void main(String[] args) {
        SpringApplication.run(WdshBusinessSysApplication.class, args);
    }

}
