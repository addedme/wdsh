package com.haifeng.wdsh.business.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haifeng.wdsh.common.base.vo.UserVo;
import com.haifeng.wdsh.common.datasource.entity.User;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author Haifeng
 * @since 2020-05-06
 */
public interface UserMapper extends BaseMapper<User> {

    /**
     * 根据用户名获取用户信息
     * @param username 用户名
     * @return
     */
    UserVo getUserByUsername(String username);
}
