package com.haifeng.wdsh.business.sys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haifeng.wdsh.business.sys.mapper.RoleMapper;
import com.haifeng.wdsh.business.sys.service.RoleService;
import com.haifeng.wdsh.common.datasource.entity.Role;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色表 服务实现类
 * </p>
 *
 * @author Haifeng
 * @since 2020-05-06
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements RoleService {

}
