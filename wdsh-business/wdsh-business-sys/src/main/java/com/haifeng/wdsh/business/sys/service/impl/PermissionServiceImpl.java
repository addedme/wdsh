package com.haifeng.wdsh.business.sys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haifeng.wdsh.business.sys.mapper.PermissionMapper;
import com.haifeng.wdsh.business.sys.service.PermissionService;
import com.haifeng.wdsh.common.datasource.entity.Permission;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 权限表 服务实现类
 * </p>
 *
 * @author Haifeng
 * @since 2020-05-06
 */
@Service
public class PermissionServiceImpl extends ServiceImpl<PermissionMapper, Permission> implements PermissionService {

}
