package com.haifeng.wdsh.business.sys.controller;

import com.haifeng.wdsh.common.base.util.Rest;
import com.haifeng.wdsh.common.base.vo.UserVo;
import com.haifeng.wdsh.business.sys.service.UserService;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  用户管理
 * </p>
 *
 * @author: Haifeng
 * @date: 2020-05-06
 */
@Api(tags = "用户管理")
@RestController
@RequestMapping("/user")
@AllArgsConstructor
public class UserController extends BaseController{

    private final UserService userService;

    @GetMapping("/getUserByUsername")
    Rest<UserVo> getUserByUsername(@RequestParam("username") String username){
        return Rest.success(userService.getUserByUsername(username));
    }
}
