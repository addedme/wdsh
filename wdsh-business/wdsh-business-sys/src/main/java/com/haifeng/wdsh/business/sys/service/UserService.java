package com.haifeng.wdsh.business.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haifeng.wdsh.common.base.vo.UserVo;
import com.haifeng.wdsh.common.datasource.entity.User;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author Haifeng
 * @since 2020-05-06
 */
public interface UserService extends IService<User> {

    /**
     * 根据用户名获取用户信息
     * @param username 用户名
     * @return
     */
    UserVo getUserByUsername(String username);
}
