package com.haifeng.wdsh.business.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haifeng.wdsh.common.datasource.entity.Dept;

/**
 * <p>
 * 部门表 Mapper 接口
 * </p>
 *
 * @author Haifeng
 * @since 2020-05-06
 */
public interface DeptMapper extends BaseMapper<Dept> {

}
