package com.haifeng.wdsh.business.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haifeng.wdsh.common.datasource.entity.Dept;

/**
 * <p>
 * 部门表 服务类
 * </p>
 *
 * @author Haifeng
 * @since 2020-05-06
 */
public interface DeptService extends IService<Dept> {

}
