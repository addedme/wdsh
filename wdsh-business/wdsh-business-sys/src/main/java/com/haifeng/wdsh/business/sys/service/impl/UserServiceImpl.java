package com.haifeng.wdsh.business.sys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haifeng.wdsh.business.sys.mapper.UserMapper;
import com.haifeng.wdsh.business.sys.service.UserService;
import com.haifeng.wdsh.common.base.vo.UserVo;
import com.haifeng.wdsh.common.datasource.entity.User;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author Haifeng
 * @since 2020-05-06
 */
@Service
@AllArgsConstructor
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    private final UserMapper userMapper;

    /**
     * 根据用户名获取用户信息
     * @param username 用户名
     * @return
     */
    @Override
    public UserVo getUserByUsername(String username) {
        return userMapper.getUserByUsername(username);
    }
}
