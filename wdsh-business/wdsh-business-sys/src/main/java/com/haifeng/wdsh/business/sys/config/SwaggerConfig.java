package com.haifeng.wdsh.business.sys.config;

import com.github.xiaoymin.knife4j.spring.annotations.EnableKnife4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * <p>
 *  Swagger-系统模块
 * </p>
 *
 * @author: Haifeng
 * @date: 2020-05-06
 */
@Configuration
@Profile({"dev","test"})
@EnableSwagger2
@EnableKnife4j
@Import(BeanValidatorPluginsConfiguration.class)
public class SwaggerConfig {

    @Bean(value = "系统模块")
    @Order(value = 1)
    public Docket groupRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(groupApiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.haifeng.wdsh.business.sys.controller"))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo groupApiInfo(){
        String name = "Haifeng";
        String url = "";
        String email = "haifeng_personal@aliyun.com";
        return new ApiInfoBuilder()
                .title("WDSH系统接口文档")
                .description("<div style='font-size:14px;color:red;'>仅供开发、测试环境使用</div>")
                .termsOfServiceUrl("")
                .contact(new Contact(name,url,email))
                .version("1.0")
                .build();
    }
}
