package com.haifeng.wdsh.business.sys.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 角色表 前端控制器
 * </p>
 *
 * @author Haifeng
 * @since 2020-05-06
 */
@RestController
@RequestMapping("/role")
public class RoleController extends BaseController {

}

