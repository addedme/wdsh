package com.haifeng.wdsh.business.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haifeng.wdsh.common.datasource.entity.Role;

/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @author Haifeng
 * @since 2020-05-06
 */
public interface RoleService extends IService<Role> {

}
